import { parse as htmlParse } from "node-html-parser"
import hashAnalyze from "./hash.js"

/**
 * Recursively searches through an array and returns all sub-arrays whose at least one element passes `filter` function
 * @param a
 * @param filter
 * @returns {*[]}
 */
function findSubArray(arr, filter) {
    let results = [];

    for (const i in arr) {
        if (arr[i].constructor === Array) {
            results = results.concat(...findSubArray(arr[i], filter))
        } else {
            if (filter(i, arr[i])) {
                results.push(arr)
            }
        }
    }

    return results
}

function parseBonus(tooltipData) {
    const possibleBonuses = {
        // Polish
        "Obrażenia": "damage",
        "Pancerz": "armor",
        "Siła": "strength",
        "Władanie bronią": "dexterity",
        "Zręczność": "agility",
        "Budowa fizyczna": "constitution",
        "Charyzma": "charisma",
        "Inteligencja": "intelligence",

        // UK English
        "Damage": "damage",
        "Armour": "armor",
        "Strength": "strength",
        "Dexterity": "dexterity",
        "Agility": "agility",
        "Constitution": "constitution",
        "Charisma": "charisma",
        "Intelligence": "intelligence",

        // US English
        "Armor": "armor",

        // Potentially other languages...
    }
    const potentialBonuses = findSubArray(tooltipData, (k, v) => v === "#00B712")
    for (const b of potentialBonuses) {
        // Is it temporary/time-limited bonus? Those have some HTML text in their descriptions, which let's us skip them easily
        if (b.match(/[(<=#]/)) {
            continue
        }

        // Parses "+X «skill»" bonus description
        const parse = /\+(\d+)\s(.+)/g.exec(b.trim())
        if (!parse) {
            continue
        }

        // Possibly we don't have that bonus in the mapping array above. Not supported language? Gracefully ignore then
        if (typeof possibleBonuses[parse[2]] === "undefined") {
            continue
        }

        return {bonus: possibleBonuses[parse[2]], bonusValue: parseInt(parse[1])}
    }

    return {bonus: null, bonusValue: 0}
}

export default function parse(html) {
    const characterStats = { items: {} };

    const root = htmlParse.parse(html)
    characterStats.level = parseInt(root.querySelector("#char_level").textContent)
    characterStats.strength = JSON.parse(root.querySelector("#char_f0_tt").getAttribute("data-tooltip"))[0][1][0][1]
    characterStats.dexterity = JSON.parse(root.querySelector("#char_f1_tt").getAttribute("data-tooltip"))[0][1][0][1]
    characterStats.agility = JSON.parse(root.querySelector("#char_f2_tt").getAttribute("data-tooltip"))[0][1][0][1]
    characterStats.constitution = JSON.parse(root.querySelector("#char_f3_tt").getAttribute("data-tooltip"))[0][1][0][1]
    characterStats.charisma = JSON.parse(root.querySelector("#char_f4_tt").getAttribute("data-tooltip"))[0][1][0][1]
    characterStats.intelligence = JSON.parse(root.querySelector("#char_f5_tt").getAttribute("data-tooltip"))[0][1][0][1]

    const items = root.querySelectorAll("[data-hash]")
    for (const item of items) {
        const itemData = hashAnalyze(item.getAttribute("data-hash"))
        const tooltip = JSON.parse(item.getAttribute("data-tooltip").replace(/"icon_gold"/, ''))

        const bonus = parseBonus(tooltip)

        characterStats.items[item.getAttribute("data-container-number")] = {
            base: item.getAttribute("data-basis"),
            prefix_id: itemData.prefix_id,
            suffix_id: itemData.suffix_id,
            quality: item.getAttribute("data-quality"),
            ...bonus,
        }
    }

    return characterStats
}
