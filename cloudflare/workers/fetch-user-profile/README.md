# Fetch player data - Cloudflare worker

The feature of pre-loading user data into character planner requires
backend functionalities to scrape the data from the public player profile.

This code exposes secure endpoint with Cloudflare Workers, which provides the user data.
The endpoint is heavily rate limited to prevent any unnecessary loads from GF servers.

```http request
GET /?server=s99-en&user_id=123123

Response:
{
  "items": {
    "2": {"base":"4-1","prefix_id":122,"suffix_id":215},
    "3":{"base":"1-1","prefix_id":118,"suffix_id":76},
    "4":{"base":"2-1","prefix_id":122,"suffix_id":215},
    "5":{"base":"3-2","prefix_id":122,"suffix_id":215},
    "6":{"base":"6-1","prefix_id":122,"suffix_id":96},
    "7":{"base":"6-1","prefix_id":118,"suffix_id":96},
    "9":{"base":"5-1","prefix_id":122,"suffix_id":187},
    "10":{"base":"8-1","prefix_id":122,"suffix_id":163},
    "11":{"base":"9-1","prefix_id":122,"suffix_id":186}
  },
  "level":"121",
  "strength":497,
  "dexterity":597,
  "agility":600,
  "constitution":567,
  "charisma":593,
  "intelligence":600
}
```

## Building
`esbuild` is used to bundle the whole codebase into one file, `dist/worker.js`.
That file can be uploaded to Cloudflare Worker to provide the endpoint.
```shell
yarn run build
```

