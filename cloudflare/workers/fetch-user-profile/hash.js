// Simple Hash Part Decode
// Source: https://forum.gladiatus.gameforge.com/forum/thread/1995-prefixes-suffixes-and-materials/?postID=80543#post80543
const hash_decode = function(hash) {
    var key = ['0','1','2','3','4','5','6','7','8','9','a','b','c','d','e','f','g','h','i','j','k','l','m','n','o','p','q','r','s','t','u','v','w','x','y','z'];
    var code = 0;
    for (var i = hash.length - 1; i >= 0; i--) {
        code += key.indexOf(hash[i]) * Math.pow(key.length, hash.length - i - 1);
    }
    return code;
};
// Full Hash Decode (gear items only)
const hash_analyze = function(hash) {
    var parts = hash.split('-');
    for (var i = 0; i < parts.length; i++) {
        parts[i] = hash_decode(parts[i]);
    }

    return {
        player_id : parts[0],
        category_id : parts[1],
        subcategory_id : parts[2],
        price_gold : parts[3],
        unknown_part_4 : parts[4],
        prefix_id : parts[5],
        suffix_id : parts[6],
        unknown_part_7 : parts[7],
        sold_flag : parts[8],
        unknown_part_9 : parts[9],
        unknown_part_10 : parts[10],
        unknown_part_11 : parts[11],
        unknown_part_12 : parts[12],
        unknown_part_13 : parts[13],
        unknown_part_14 : parts[14],
        durability : parts[15],
        unknown_part_16 : parts[16],
        soulbound_player_id : parts[17]
    };
};

export default hash_analyze
