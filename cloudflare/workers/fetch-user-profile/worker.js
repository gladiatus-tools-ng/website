import parseCharacter from "./parse_character.js"

export default {
    async fetch(request, env, ctx) {
        async function corsResponse(response) {
            response.headers.set("Access-Control-Allow-Origin", "https://gladiatus-toolbox.net");
            return response
        }

        async function handleRequest(request) {

            const url = new URL(request.url);
            const server = url.searchParams.get("server");
            const userId = url.searchParams.get("user_id");
            const doll = url.searchParams.get("doll");

            if(!server || !userId || !server.match(/^s\d+-[a-z]{2}$/) || !userId.match(/^\d+$/)) {
                return corsResponse(new Response(null, {status: 400}))
            }

            if(doll && !doll.match(/^[1-6]$/)) {
                return corsResponse(new Response(null, {status: 400}))
            }

            let resp = null
            try {
                resp = await fetch(`https://${server}.gladiatus.gameforge.com/game/index.php?mod=player&p=${userId}&doll=${doll ?? 1}`)
            } catch (e) {
                return corsResponse(new Response(null, {status: 500}))
            }

            if(!resp.ok) {
                return corsResponse(new Response(null, {status: 502}))
            }

            const profileSource = await resp.text()
            const profileData = parseCharacter(profileSource)

            const response = Response.json(profileData)
            return corsResponse(response)
        }

        async function handleOptions(request) {
            // Handle CORS preflight requests.
            return new Response(null, {
                headers: {
                    "Access-Control-Allow-Origin": "https://gladiatus-toolbox.net",
                    "Access-Control-Allow-Methods": "GET,OPTIONS",
                    "Access-Control-Max-Age": "86400",
                },
            });
        }

        if (request.method === "OPTIONS") {
            // Handle CORS preflight requests
            return handleOptions(request);
        } else if (request.method === "GET") {
            // Handle requests to the API server
            return handleRequest(request);
        } else {
            return new Response(null, {
                status: 405,
                statusText: "Method Not Allowed",
            });
        }
    },
};
