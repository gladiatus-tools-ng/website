export default {
    name: "Progress",
    template: `<div class="bg-gray-200 w-full h-1.5 rounded" aria-hidden="true"><div class="rounded bg-green-600 h-1.5" :style="{width: value+'%'}"></div></div>`,
    props: {
        bgColor: { type: String, default: "bg-gray-300" },
        fgColor: { type: String, default: "bg-green-600" },
        value: Number
    }
}
