import { toRaw } from "vue";
import { statAsString } from "../utils";
import { calculateItemStats } from "../item";
import baseStats from "../data/base_stats.json"
import { statsColumnsMapping } from "../utils"

export default {
    name: 'ItemEditor',
    template: `
      <div class="bg-gray-100 py-3 px-4 rounded-sm w-full lg:w-1/3 lg:min-h-16">
        <!-- Preview -->
        <div class="font-bold cursor-pointer flex items-center" @click="edit=true" role="button" v-if="!edit">
          <span class="text-gray-500 mr-2" v-show="base === null">{{ $t("base-category-" + baseCategory) }}:</span>
          <div class="icon-edit w-4 h-4 min-w-4 inline-block mr-1" :style="{background: nameColor}"></div>
          <span :style="{color: nameColor}">
            {{ base ? itemName(prefixId, base, suffixId) : $t("No item") }}
          </span>
        </div>
        <div v-if="!edit && base">
          <div v-if="base && itemData.isWeapon">
            {{ $t("stat-damage") }}
            {{ minCap(Math.floor(itemData.damageLow)) + "-" + Math.floor(itemData.damageHigh) }}
          </div>
          <div v-for="(v,k) in statsColumnsMapping" v-if="base">
            <span v-if="itemData.stats[v] !== 0 && k !== 'name'">
              {{ statAsString(itemData.stats[v], k, 1, $t("stat-" + stripPercentageFromName(k))) }}
            </span>
          </div>
        </div>
        <div v-if="!edit && bonus" class="text-green-500">
          {{ statAsString(bonusValue, bonus, 1, $t("stat-" + bonus)) }}
        </div>

        <!-- Editor -->
        <div class="flex flex-col gap-y-2" v-if="edit">
          <select v-model="prefixId" class="bg-gray-200 rounded" @change="update()">
            <option :value="0"></option>
            <option v-for="prefix in prefixesStats" :value="prefix.id">{{ prefix.name }}</option>
          </select>
          <div class="flex gap-2">
            <select v-model="quality" class="bg-gray-200 rounded w-1/2" :style="{color: qualityColors[quality]}">
              <option :value="5">Olimp +</option>
              <option :value="4">Olimp / Jupiter+</option>
              <option :value="3">Jupiter / Mars+</option>
              <option :value="2">Mars / Neptun +</option>
              <option :value="1">Neptun / Ceres +</option>
              <option :value="0">Ceres</option>
            </select>
            <select v-model="base" class="bg-gray-200 rounded w-1/2" @change="update()">
              <option :value="null"></option>
              <option v-for="base in bases" :value="base">{{ $t("item-" + base) }}</option>
            </select>
          </div>
          
          <select v-model="suffixId" class="bg-gray-200 rounded" @change="update()">
            <option :value="0"></option>
            <option v-for="suffix in suffixesStats" :value="suffix.id">{{ suffix.name }}</option>
          </select>


          <div class="flex w-full gap-2">
            <select v-model="bonus" class="bg-gray-200 rounded w-2/3">
              <option :value="null"></option>
              <option v-if="itemData.isWeapon" value="damage">+{{ $t("Damage") }}</option>
              <option v-if="itemData.isArmor" value="armor">+{{ $t("Armor") }}</option>
              <option value="strength">+{{ $t("Strength") }}</option>
              <option value="dexterity">+{{ $t("Dexterity") }}</option>
              <option value="agility">+{{ $t("Agility") }}</option>
              <option value="constitution">+{{ $t("Constitution") }}</option>
              <option value="charisma">+{{ $t("Charisma") }}</option>
              <option value="intelligence">+{{ $t("Intelligence") }}</option>
            </select>
            <input type="number" class="w-1/3 bg-gray-200 p-1" min="0" v-model="bonusValue">
          </div>

          <button class="bg-gray-200 px-4 py-2 rounded-md mt-2 sm:mt-0"
                  @click="edit=false; $emit('update', {prefixId, base, suffixId, quality, bonus, bonusValue})">
            {{ $t("Save") }}
          </button>
          <button class="bg-gray-200 px-4 py-2 rounded-md mt-2 sm:mt-0" @click="edit=false; reset()">
            {{ $t("Cancel") }}
          </button>
        </div>
      </div>
    `,
    props: ["item", "baseCategory", "prefixesStats", "suffixesStats"],
    data() {
        return {
            statsColumnsMapping,
            prefixId: toRaw(this.item.prefixId),
            base: toRaw(this.item.base),
            suffixId: toRaw(this.item.suffixId),
            quality: toRaw(this.item.quality) ?? 0,
            bonus: toRaw(this.item.bonus) ?? null,
            bonusValue: toRaw(this.item.bonusValue) ?? 0,
            itemData: this.base ? calculateItemStats(this.item.prefixId, this.item.base, this.item.suffixId, this.item.quality) : {},
            edit: false,
            bases: Object.keys(baseStats).filter(base => base.match(this.baseCategory + "-")),
            qualityColors: {
                5: "#ff0000", 4: "#ff0000", 3: "#ff6a00", 2: "#e303e0", 1: "#5159f7", 0: "#00b700",
            },
        }
    },
    methods: {
        statAsString,
        minCap(i) {
            return i > 0 ? i : 1
        },

        update() {
            if (!this.base) {
                return
            }
            this.itemData = calculateItemStats(this.prefixId, this.base, this.suffixId, this.quality)
        },

        reset() {
            this.prefixId = this.item.prefixId
            this.base = this.item.base
            this.suffixId = this.item.suffixId
            this.quality = this.item.quality
            this.bonus = null
            this.bonusValue = 0
        },

        itemName(prefixId, base, suffixId) {
            let name = prefixId ? this.$t("prefix-" + prefixId) + " " : ""
            name += this.$t("item-" + base)
            name += suffixId ? " " + this.$t("suffix-" + suffixId) : ""

            return name
        },

        stripPercentageFromName(stat) {
            return stat.match(/Percentage$/) ? stat.replace("Percentage", "") : stat
        },

    },

    watch: {
        item: {
            handler(v) {
                this.prefixId = toRaw(v.prefixId)
                this.base = toRaw(v.base)
                this.suffixId = toRaw(v.suffixId)
                this.quality = toRaw(v.quality) ?? "0"
                this.bonus = toRaw(v.bonus) ?? null
                this.bonusValue = toRaw(v.bonusValue) ?? 0
                this.update()
            },
            deep: true,
        }
    },

    computed: {
        nameColor() {
            return this.base !== null ? this.qualityColors[this.quality] : 'rgb(107, 114, 128)'
        }
    }
}
