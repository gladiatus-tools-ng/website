import Progress from "./ProgressBar"
import { toRaw } from "vue";

export default {
    name: "StatOverview",
    components: { Progress },
    template: `
      <div class="flex flex-col">
        <div class="flex">
          {{ name }}
          <div class="ml-auto">{{ stat.total }}</div>
        </div>
        <Progress :value="stat.total / stat.max * 100" class="col-span-3"></Progress>

        <label class="text-gray-400 text-xs flex mt-0.5 flex items-center">
          {{ $t("Base") }}
          <input type="number" class="ml-auto w-16 p-1 bg-gray-200" v-model="baseStat"
                 @input="$emit('update', baseStat)" min="5"
                 :max="level < 40 ? 200 : level * 5">
        </label>
        <div class="text-gray-400 text-xs flex" v-if="detailed">
          {{ $t("Max") }}
          <div class="ml-auto">{{ stat.max }}</div>
        </div>
        <div class="text-gray-400 text-xs text-right flex" v-if="detailed">
          {{ $t("Through items") }}
          <div class="ml-auto">
            {{ stat.total - stat.base }} {{ $t("out of") }} {{ stat.throughItems }}
          </div>
        </div>
      </div>
    `,
    props: {
        stat: Object,
        level: Number,
        detailed: Boolean,
        name: String,
    },
    data() {
        return {
            baseStat: toRaw(this.stat.base)
        }
    },
    watch: {
        stat: {
            handler: function(){
                this.baseStat = toRaw(this.stat.base)
            },
            deep: true,
        }
    }
}
