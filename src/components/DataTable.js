import { getObjectDeepProperty } from "../utils";
import { shallowRef } from "vue";

export default {
    template: `
      <div class="overflow-y-auto flex flex-col">
        <div class="flex my-2 items-center flex-col md:flex-row">
          <label v-if="allowHeaderSwitch" class="ml-auto md:min-w-32 lg:min-w-52 text-right md:order-2">
            <input type="checkbox" v-model="tableHeaderVertical">
            Vertical table header
          </label>

          <div class="w-full md:mr-4 px-0.5 md:pl-0.5" v-show="filterable">
            <label v-show="false" for="filter">{{ $t("Filter") }}</label>
            <input id="filter" type="text"
                   v-model="filterRaw"
                   class="bg-gray-100 rounded w-full p-2 px-4"
                   :placeholder="$t('Search...')">
          </div>
        </div>

        <table class="w-full border dark:border-gray-500 p-4 stripped text-center" style="position:relative">
          <thead class="dark:bg-gray-800">
          <tr class="nowrap cursor-pointer" :class="{vertical: tableHeaderVertical, horizontal: !tableHeaderVertical}">
            <th v-for="column in columns"
                :title="columnsLabels[column]"
                @click="changeSort(column)">
              {{ columnsLabels[column] }}
              <div v-if="primarySortColumn === column" style="display:inline-block">
                {{ primarySortOrder === 1 ? "▴" : "▾" }}
              </div>
            </th>
          </tr>
          </thead>
          <tbody>
          <tr v-for="row in workableData.slice(0, maxRows > -1 ? maxRows : workableData.length)" v-show="filtered(row)">
            <td v-for="column in columns" @mouseenter="addCellTitle">
              <a :href="row[column].link"
                 class="underline"
                 v-if="typeof row[column] === 'object' && row[column].hasOwnProperty('link') && row[column].hasOwnProperty('label')"
              >
                {{ row[column].label }}
              </a>
              <div v-else-if="typeof row[column] === 'object' && row[column].hasOwnProperty('html')"
                   v-html="row[column].html"></div>
              <span v-else>{{ row[column] }}</span>
            </td>
          </tr>
          </tbody>
        </table>
      </div>`,
    props: {
        columns: Object,
        columnsLabels: Object,
        data: Array,
        allowHeaderSwitch: {
            type: Boolean,
            default: true,
        },
        defaultSortColumn: String,
        defaultSortOrder: Number,
        maxRows: {
            type: Number,
            default: -1
        },
        filterable: {
            type: Boolean,
            default: false,
        },
        filterableColumns: {
            type: Array,
            default: []
        },
    },
    data() {
        return {
            primarySortColumn: null,
            primarySortOrder: 1,
            workableData: [],

            tableHeaderVertical: false,
            filterRaw: "",
            filter: "",
        }
    },
    methods: {
        changeSort(column) {
            this.primarySortOrder = (this.primarySortColumn === column) ? this.primarySortOrder * -1 : -1;
            this.primarySortColumn = column

            this.sort()
        },


        sort() {
            this.workableData = this.workableData.sort(this.primarySortColumn === "name" ? (a, b) => {
                return this.sortByName(a, b)
            } : (a, b) => {
                // Second-level ordering by name (if it exists) when sorting column is the same
                if (parseInt(a[this.primarySortColumn]) === parseInt(b[this.primarySortColumn]) && a.hasOwnProperty("name")) {
                    return this.sortByName(a, b)
                }

                return (parseInt(a[this.primarySortColumn]) < parseInt(b[this.primarySortColumn]) ? -1 : 1) * this.primarySortOrder
            })
        },

        sortByName(a, b) {
            if (typeof a.name === 'object' && a.name.hasOwnProperty("label")) {
                return a.name.label.localeCompare(b.name.label) * this.primarySortOrder
            } else {
                return a.name.localeCompare(b.name) * this.primarySortOrder
            }
        },

        addCellTitle(e) {
            const columnId = Array.prototype.indexOf.call(e.target.parentNode.children, e.target);
            e.target.title = this.columnsLabels[this.columns[columnId]]
        },

        filtered(row) {
            if (!this.filterable) {
                return true;
            }

            return row.searchable?.includes(this.filter)
        },

        prepareWorkableData(data) {
            const workableData = [...data]

            if (!this.filterable) {
                return workableData
            }

            workableData.forEach((row, i) => {
                const filterableColumns = this.filterableColumns.map(col => getObjectDeepProperty(row, col)).join(" ")

                workableData[i].searchable = filterableColumns
                    .toLowerCase()
                    .normalize("NFKD")
                    .replace(/[\u0300-\u036f]/g, "")
            })

            return workableData
        }
    },

    mounted() {
        this.workableData = this.prepareWorkableData(this.data)
        this.sort()
    },

    created() {
        this.primarySortColumn = this.defaultSortColumn ?? null;
        this.primarySortOrder = this.defaultSortOrder === -1 ? -1 : 1;
        this.sort()
    },

    watch: {
        data(newData) {
            this.workableData = this.prepareWorkableData(newData)
            this.sort()
        },

        filterRaw(newVal) {
            this.filter = newVal.toLowerCase().normalize("NFKD").replace(/[\u0300-\u036f]/g, "")
        }
    }
}
