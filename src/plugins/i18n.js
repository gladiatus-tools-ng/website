import pl from "../locale/pl.json"
import en from "../locale/en.json"
import tr from "../locale/tr.json"

const availableLocales = { pl, en, tr }
export default {
    install: (app, options) => {
        app.config.globalProperties.language = localStorage.getItem("language") ?? "en"
        const locale = availableLocales[app.config.globalProperties.language]
        app.config.globalProperties.$t = (msg) => locale.hasOwnProperty(msg) ? locale[msg] : msg
    },
}
