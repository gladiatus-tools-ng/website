export default {
    name: "Credits",
    template: `
      <main class="w-full flex-grow p-6" v-cloak>
        <h1 class="text-3xl text-black pb-6">{{$t("Credits")}}</h1>


        <div class="bg-white p-6 px-5 rounded-md ">
            <b>Most of the data available on this website comes from the following sources:</b>
            <ul class="list-disc ml-6 mt-2">
                <li><a href="https://github.com/DinoDevs/GladiatusCrazyAddon" target="_blank" class="underline">🔗 Gladiatus Crazy Addon</a> by DinoDevs</li>
                <li><a href="https://gladiatus-tools.com" target="_blank" class="underline">🔗 Gladiatus Tools (defunct)</a> by Michalus</li>
                <li><a href="https://gladiatus.gamerz-bg.com/" target="_blank" class="underline">🔗 Gladiauts Fansite</a> by Skarsburning</li>
            </ul>
            <p class="mt-2">
                The tools available here were directly inspired by Gladiatus Tools. 
            </p>
            <p class="mt-2">
                Thanks to everyone contributing over the years, without your work this website would not be possible.
            </p>
            
            <p class="mt-4">
                Original work constitutes:
            </p>
            <ul class="list-disc ml-6 mt-2">
                <li>Reverse-engineering formulas for weapon damage</li>
                <li>Reverse-engineering formula for items armor</li>
                <li>Completing and cleaning up data (mostly affixes stats)</li>
                <li>Putting the website together (<a href="https://gladiatus-toolbox.net" class="underline">https://gladiatus-toolbox.net</a>)</li>
            </ul>
            
            <p class="mt-4">
                All the source code and data available on this website is open source and available at <a href="https://gitlab.com/gladiatus-tools-ng/website" target="_blank" class="underline">GitLab</a>.
                Bug reports and other contributions are welcome and appreciated.
                The GitLab repository is also the only and recommended way to get in touch with me – feel free to just open a new issue.
            </p>
            
            <p class="mt-4 mb-2">
                Lastly, if you find the tools valuable, you can buy me a coffee. Thanks!
            </p>
            <div class="w-fit">
                <a href="https://www.buymeacoffee.com/gladiatus.toolbox" target="_blank"><img src="https://cdn.buymeacoffee.com/buttons/v2/default-yellow.png" alt="Buy Me A Coffee" style="height: 60px !important;width: 217px !important;" ></a>      
            </div>
            
            
        </div>
      </main>`,
}
