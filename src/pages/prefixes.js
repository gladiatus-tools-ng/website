import DataTable from "../components/DataTable.js";
import { statAsString, statsColumnsMapping, mapObject } from "../utils";
import prefixesStats from "../data/prefixes_stats.json"

export default {
    name: "Prefixes",
    components: { DataTable },
    template: `
      <main class="w-full flex-grow p-6" v-cloak>
        <h1 class="text-3xl text-black pb-6 ">{{ $t("Prefixes") }}</h1>

        <div class="bg-white py-4 px-6 rounded-md ">
          <DataTable
              :columns="columns"
              :columns-labels="columnsLabels"
              :data="prefixesStatsWithNames"
              :filterable="true"
              :filterable-columns="['name.label', 'level']"
              default-sort-column="name"
          ></DataTable>
        </div>
      </main>
    `,

    data() {
        return {
            columns: Object.keys(statsColumnsMapping),
            columnsLabels: mapObject(statsColumnsMapping, (r, key) => r[key] = this.$t("stat-" + key)),
            prefixesStatsWithNames: Object.keys(prefixesStats).map(prefixId => {
                // Convert raw stats array into `stat-name => value` object
                const stats = mapObject(
                    statsColumnsMapping,
                    (r, key) => r[key] = statAsString(prefixesStats[prefixId][statsColumnsMapping[key]], key)
                )
                stats["name"] = {
                    link: `?p=items-builder#${prefixId},1-1,0`,
                    label: this.$t("prefix-" + prefixId)
                }
                return stats
            }),
        }
    },

}
