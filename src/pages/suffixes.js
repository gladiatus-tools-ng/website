import DataTable from "../components/DataTable.js";
import { statAsString, statsColumnsMapping, mapObject } from "../utils";
import excludedSuffixes from "../data/excluded_suffixes.json"
import suffixesStats from "../data/suffixes_stats.json"

export default {
    name: "Suffixes",
    components: { DataTable },
    template: `
      <main class="w-full flex-grow p-6" v-cloak>
        <h1 class="text-3xl text-black pb-6">{{ $t("Suffixes") }}</h1>

        <div class="bg-white py-4 px-6 rounded-md">
          <DataTable
              :columns="columns"
              :columns-labels="columnsLabels"
              :data="suffixesStatsWithNames"
              :filterable="true"
              :filterable-columns="['name.label', 'level']"
              default-sort-column="name"></DataTable>
        </div>
      </main>
    `,

    data() {
        return {
            columns: Object.keys(statsColumnsMapping),
            columnsLabels: mapObject(statsColumnsMapping, (r, key) => r[key] = this.$t("stat-" + key)),
            suffixesStatsWithNames: Object.keys(suffixesStats)
                .filter(suffixId => !excludedSuffixes.includes(parseInt(suffixId)))
                .map(suffixId => {
                    // Convert raw stats array into `stat-name => value` object
                    const stats = mapObject(
                        statsColumnsMapping,
                        (r, key) => r[key] = statAsString(suffixesStats[suffixId][statsColumnsMapping[key]], key)
                    )
                    stats["name"] = {
                        link: `?p=items-builder#0,1-1,${suffixId}`,
                        label: this.$t("suffix-" + suffixId)
                    }
                    return stats
                }),
        }
    },

}
