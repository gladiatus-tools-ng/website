import baseStats from "../data/base_stats.json"
import prefixesStatsRaw from "../data/prefixes_stats.json"
import suffixesStatsRaw from "../data/suffixes_stats.json"
import excludedPrefixes from "../data/excluded_prefixes.json"
import excludedSuffixes from "../data/excluded_suffixes.json"
import { calculateItemStats } from "../item"
import { statAsString, statsColumnsMapping, sumStat } from "../utils";
import Progress from "../components/ProgressBar";
import StatOverview from "../components/StatOverview";
import { toRaw } from "vue";
import ItemEditor from "../components/ItemEditor";

export default {
    name: "CharPlanner",
    components: { ItemEditor, StatOverview, Progress },
    template: `
      <main class="w-full flex-grow py-4 px-2 sm:p-6" v-cloak>
        <h1 class="text-3xl text-black pb-6">{{ $t("Character planner") }}</h1>

        <div class="bg-white py-3 px-2 sm:p-4 rounded-md">
          <div class="bg-gray-100 rounded w-full p-4 px-5  mb-4 flex flex-col sm:flex-row">
            <label class="flex sm:gap-4 flex-1 sm:mr-2 sm:items-center flex-col sm:flex-row">
              <span class="min-w-fit">{{ $t("Profile link") }}:</span>
              <span class="spinner block" v-if="profileDataLoading"></span>
              <input type="url"
                     class="w-full p-2 px-4 bg-gray-200 disabled:animate-pulse disabled:opacity-10 disabled:cursor-not-allowed"
                     v-model="profileLink" :disabled="profileDataLoading">
            </label>
            <button
                class="bg-gray-200 px-4 py-2 rounded-md disabled:opacity-30 disabled:cursor-not-allowed mt-2 sm:mt-0"
                @click="loadUserData"
                :disabled="!profileLink || profileDataLoading"
            >
              {{ $t("Load") }}
            </button>
          </div>
          <div class="flex items-start flex-col gap-y-2 lg:flex-row" :class="{'md:flex-row': !showDetailedResults}">
            <span class="md:w-1/3 lg:w-1/4 xl:w-1/5 hidden"><!--Here just to keep w- classes from tree-shaking removal --></span>
            <div class="bg-gray-100 rounded w-full lg:w-1/2 xl:w-1/3 p-4 px-5 grid grid-cols-1 gap-4"
                 :class="{'sm:grid-cols-2': showDetailedResults, 'md:w-1/3': !showDetailedResults, 'lg:w-1/4': !showDetailedResults, 'xl:w-1/5':!showDetailedResults}">
              <div class="flex flex-col gap-y-2">
                <label><input type="checkbox" v-model="showDetailedResults"> Detailed</label>
                <div class="flex items-center">
                  <div class="flex-1">{{ $t("Level") }}</div>
                  <input type="number" v-model="playerStats.level" class="text-sm w-16 bg-gray-200 p-1 pl-2">
                </div>

                <StatOverview :stat="playerStats.strength" :detailed="showDetailedResults"
                              :name="$t('Strength')"
                              :level="playerStats.level"
                              @update="e => {playerStats.strength.base = e; calculateResults()}"></StatOverview>
                <StatOverview :stat="playerStats.dexterity" :detailed="showDetailedResults"
                              :name="$t('Dexterity')"
                              :level="playerStats.level"
                              @update="e => {playerStats.dexterity.base = e; calculateResults()}"></StatOverview>
                <StatOverview :stat="playerStats.agility" :detailed="showDetailedResults"
                              :name="$t('Agility')"
                              :level="playerStats.level"
                              @update="e => {playerStats.agility.base = e; calculateResults()}"></StatOverview>
                <StatOverview :stat="playerStats.constitution" :detailed="showDetailedResults"
                              :name="$t('Constitution')"
                              :level="playerStats.level"
                              @update="e => {playerStats.constitution.base = e; calculateResults()}"></StatOverview>
                <StatOverview :stat="playerStats.charisma" :detailed="showDetailedResults"
                              :name="$t('Charisma')"
                              :level="playerStats.level"
                              @update="e => {playerStats.charisma.base = e; calculateResults()}"></StatOverview>
                <StatOverview :stat="playerStats.intelligence" :detailed="showDetailedResults"
                              :name="$t('Intelligence')"
                              :level="playerStats.level"
                              @update="e => {playerStats.intelligence.base = e; calculateResults()}"></StatOverview>
              </div>

              <div class="flex flex-col gap-y-2">
                <div class="flex font-bold">
                  <div class="flex-1">{{ $t("Armor") }}</div>
                  <div>{{ playerStats.armor }}</div>
                </div>

                <div class="flex text-gray-400 text-xs" v-if="showDetailedResults">
                  <div class="flex-1">{{ $t("Absorbed damage") }}</div>
                  <div>
                    {{ Math.ceil((playerStats.armor / 74) - (playerStats.armor / 74) / 660 + 1) }}
                    -
                    {{ Math.floor(playerStats.armor / 66 + playerStats.armor / 660) }}
                  </div>
                </div>

                <!-- Hardening / resilience -->
                <div class="flex text-sm">
                  <div class="flex-1">{{ $t("Resilience") }}</div>
                  <div
                      :class="{
                  'text-green-500': playerStats.resilience.total >= playerStats.resilience.ceiling, 
                  'text-red-500': playerStats.resilience.total < playerStats.resilience.ceiling
              }">
                    {{ playerStats.resilience.total }} / {{ playerStats.resilience.ceiling }}
                  </div>
                </div>
                <div class="flex text-gray-400 text-xs" v-if="showDetailedResults">
                  <div class="flex-1">{{ $t("Through items") }}</div>
                  {{ statAsString(playerStats.resilience.throughItems) }}
                </div>
                <div class="flex text-gray-400 text-xs" v-if="showDetailedResults">
                  <div class="flex-1">{{ $t("Through agility") }}</div>
                  {{ statAsString(playerStats.resilience.throughAgility) }}
                </div>
                <div class="flex text-gray-400 text-xs" v-if="showDetailedResults">
                  <div class="flex-1">{{ $t("Chance of avoiding critical hits") }}</div>
                  {{ playerStats.avoidCriticalHitChance }}%
                </div>

                <!-- Blocking -->
                <div class="flex text-sm">
                  <div class="flex-1">{{ $t("Blocking value") }}</div>
                  <div
                      :class="{'text-green-500': playerStats.blocking.total >= playerStats.blocking.ceiling,
                                    'text-red-500': playerStats.blocking.total < playerStats.blocking.ceiling}">
                    {{ playerStats.blocking.total }} / {{ playerStats.blocking.ceiling }}
                  </div>
                </div>
                <div class="flex text-gray-400 text-xs" v-if="showDetailedResults">
                  <div class="flex-1">{{ $t("Through items") }}</div>
                  {{ statAsString(playerStats.blocking.throughItems) }}
                </div>
                <div class="flex text-gray-400 text-xs" v-if="showDetailedResults">
                  <div class="flex-1">{{ $t("Through strength") }}</div>
                  {{ statAsString(playerStats.blocking.throughStrength) }}
                </div>
                <div class="flex text-gray-400 text-xs" v-if="showDetailedResults">
                  <div class="flex-1">{{ $t("Chance of blocking") }}</div>
                  {{ playerStats.blockingChance }}%
                </div>


                <!-- Damage -->
                <div class="flex font-bold">
                  <div class="flex-1">{{ $t("Damage") }}</div>
                  {{ playerStats.damage.total.low }} - {{ playerStats.damage.total.high }}
                </div>
                <div class="flex text-gray-400 text-xs" v-if="showDetailedResults">
                  <div class="flex-1">{{ $t("Base") }}</div>
                  {{ playerStats.damage.base.low }} - {{ playerStats.damage.base.high }}
                </div>
                <div class="flex text-gray-400 text-xs" v-if="showDetailedResults">
                  <div class="flex-1">{{ $t("Through items") }}</div>
                  {{ statAsString(playerStats.damage.throughItems) }}
                </div>
                <div class="flex text-gray-400 text-xs" v-if="showDetailedResults">
                  <div class="flex-1">{{ $t("Through strength") }}</div>
                  {{ statAsString(playerStats.blocking.throughStrength) }}
                </div>

                <!-- Critical hit -->
                <div class="flex text-sm">
                  <div class="flex-1">{{ $t("Critical damage") }}</div>
                  <div
                      :class="{'text-green-500': playerStats.criticalHit.total >= playerStats.criticalHit.ceiling,
                  'text-red-500': playerStats.criticalHit.total < playerStats.criticalHit.ceiling}">
                    {{ playerStats.criticalHit.total }} / {{ playerStats.criticalHit.ceiling }}
                  </div>
                </div>
                <div class="flex text-gray-400 text-xs" v-if="showDetailedResults">
                  <div class="flex-1">{{ $t("Through items") }}</div>
                  {{ statAsString(playerStats.criticalHit.throughItems) }}
                </div>
                <div class="flex text-gray-400 text-xs" v-if="showDetailedResults">
                  <div class="flex-1">{{ $t("Through dexterity") }}</div>
                  {{ statAsString(playerStats.criticalHit.throughDexterity) }}
                </div>
                <div class="flex text-gray-400 text-xs" v-if="showDetailedResults">
                  <div class="flex-1">{{ $t("Chance for critical damage") }}</div>
                  {{ playerStats.criticalHitChance }}%
                </div>

                <!-- Threat -->
                <div class="flex font-bold">
                  <div class="flex-1">{{ $t("Threat") }}</div>
                  {{ playerStats.threat.total }}
                </div>
                <div class="flex text-gray-400 text-xs" v-if="showDetailedResults">
                  <div class="flex-1">{{ $t("Through items") }}</div>
                  {{ statAsString(playerStats.threat.throughItems) }}
                </div>
                <div class="flex text-gray-400 text-xs" v-if="showDetailedResults">
                  <div class="flex-1">{{ $t("Through charisma") }}</div>
                  {{ statAsString(playerStats.threat.throughCharisma) }}
                </div>

                <!-- Healing -->
                <div class="flex font-bold">
                  <div class="flex-1">{{ $t("Healing") }}</div>
                  {{ playerStats.healing.total }}
                </div>
                <div class="flex text-gray-400 text-xs" v-if="showDetailedResults">
                  <div class="flex-1">{{ $t("Through items") }}</div>
                  {{ statAsString(playerStats.healing.throughItems) }}
                </div>
                <div class="flex text-gray-400 text-xs" v-if="showDetailedResults">
                  <div class="flex-1">{{ $t("Through intelligence") }}</div>
                  {{ statAsString(playerStats.healing.throughIntelligence) }})
                </div>

                <!-- Critical healing -->
                <div class="flex text-sm">
                  <div class="flex-1">{{ $t("Critical healing value") }}</div>
                  <div
                      :class="{'text-green-500': playerStats.criticalHealing.total >= playerStats.criticalHealing.ceiling,
                  'text-red-500': playerStats.criticalHealing.total < playerStats.criticalHealing.ceiling}">
                    {{ playerStats.criticalHealing.total }} / {{ playerStats.criticalHealing.ceiling }}
                  </div>
                </div>
                <div class="flex text-gray-400 text-xs" v-if="showDetailedResults">
                  <div class="flex-1">{{ $t("Through items") }}</div>
                  {{ statAsString(playerStats.criticalHealing.throughItems) }}
                </div>
                <div class="flex text-gray-400 text-xs" v-if="showDetailedResults">
                  <div class="flex-1">{{ $t("Through intelligence") }}</div>
                  {{ statAsString(playerStats.criticalHealing.throughIntelligence) }}
                </div>
                <div class="flex text-gray-400 text-xs" v-if="showDetailedResults">
                  <div class="flex-1">{{ $t("Chance of critical healing") }}</div>
                  {{ statAsString(playerStats.criticalHealingChance) }}%
                </div>
              </div>
            </div>

            <div class="flex flex-col gap-y-2 w-full lg:flex-1 lg:ml-4">
              <div class="w-full flex gap-y-2 flex-col lg:gap-x-2 lg:flex-row lg:justify-end">
                <div class="hidden lg:block lg:w-1/3"></div>
                <!-- Helmet -->
                <ItemEditor
                    :item="playerItems[2]"
                    :suffixes-stats="suffixesStats"
                    :prefixes-stats="prefixesStats"
                    :base-category="4"
                    @update="item => updateItem(2, item)"
                ></ItemEditor>

                <!-- Necklace -->
                <ItemEditor
                    :item="playerItems[11]"
                    :suffixes-stats="suffixesStats"
                    :prefixes-stats="prefixesStats"
                    :base-category="9"
                    @update="item => updateItem(11, item)"
                ></ItemEditor>
              </div>
              <div class="w-full flex gap-y-2 flex-col lg:gap-x-2 lg:flex-row lg:justify-end">
                <!-- Weapon -->
                <ItemEditor
                    :suffixes-stats="suffixesStats"
                    :prefixes-stats="prefixesStats"
                    :base-category="1"
                    :item="playerItems[3]"
                    @update="item => updateItem(3, item)"
                ></ItemEditor>

                <!-- Armor -->
                <ItemEditor
                    :item="playerItems[5]"
                    :suffixes-stats="suffixesStats"
                    :prefixes-stats="prefixesStats"
                    :base-category="3"
                    @update="item => updateItem(5, item)"
                ></ItemEditor>

                <!-- Shield -->
                <ItemEditor
                    :item="playerItems[4]"
                    :suffixes-stats="suffixesStats"
                    :prefixes-stats="prefixesStats"
                    :base-category="2"
                    @update="item => updateItem(4, item)"
                ></ItemEditor>
              </div>

              <div class="w-full flex gap-y-2 flex-col lg:gap-x-2 lg:flex-row lg:justify-end">
                <!-- Gloves -->
                <ItemEditor
                    :item="playerItems[9]"
                    :suffixes-stats="suffixesStats"
                    :prefixes-stats="prefixesStats"
                    :base-category="5"
                    @update="item => updateItem(9, item)"
                ></ItemEditor>

                <!-- Shoes -->
                <ItemEditor
                    :item="playerItems[10]"
                    :suffixes-stats="suffixesStats"
                    :prefixes-stats="prefixesStats"
                    :base-category="8"
                    @update="item => updateItem(10, item)"
                ></ItemEditor>

                <!-- Ring -->
                <ItemEditor
                    :item="playerItems[6]"
                    :suffixes-stats="suffixesStats"
                    :prefixes-stats="prefixesStats"
                    :base-category="6"
                    @update="item => updateItem(6, item)"
                ></ItemEditor>
              </div>

              <div class="w-full flex gap-y-2 flex-col lg:gap-x-2 lg:flex-row lg:justify-end">
                <div class="hidden lg:block lg:w-1/3"></div>
                <div class="hidden lg:block lg:w-1/3"></div>
                <!-- Ring #2 -->
                <ItemEditor
                    :item="playerItems[7]"
                    :suffixes-stats="suffixesStats"
                    :prefixes-stats="prefixesStats"
                    :base-category="6"
                    @update="item => updateItem(7, item)"
                ></ItemEditor>
              </div>
            </div>
          </div>
        </div>
      </main>`,

    data() {
        return {
            statsColumnsMapping,
            playerItems: { // Numbered as items slots in gladiatus code
                2: { prefixId: null, base: null, suffixId: null, stats: { stats: [] }, bonus: null, bonusValue: 0 },
                3: { prefixId: null, base: null, suffixId: null, stats: { stats: [] }, bonus: null, bonusValue: 0  },
                4: { prefixId: null, base: null, suffixId: null, stats: { stats: [] }, bonus: null, bonusValue: 0  },
                5: { prefixId: null, base: null, suffixId: null, stats: { stats: [] }, bonus: null, bonusValue: 0  },
                6: { prefixId: null, base: null, suffixId: null, stats: { stats: [] }, bonus: null, bonusValue: 0  },
                7: { prefixId: null, base: null, suffixId: null, stats: { stats: [] }, bonus: null, bonusValue: 0  },
                9: { prefixId: null, base: null, suffixId: null, stats: { stats: [] }, bonus: null, bonusValue: 0  },
                10: { prefixId: null, base: null, suffixId: null, stats: { stats: [] }, bonus: null, bonusValue: 0  },
                11: { prefixId: null, base: null, suffixId: null, stats: { stats: [] }, bonus: null, bonusValue: 0  },
            },

            playerStats: {
                level: 1,
                health: 0,
                strength: { base: 5, max: 9, throughItems: 0, total: 5 },
                dexterity: { base: 5, max: 9, throughItems: 0, total: 5 },
                agility: { base: 5, max: 9, throughItems: 0, total: 5 },
                constitution: { base: 5, max: 9, throughItems: 0, total: 5 },
                charisma: { base: 5, max: 9, throughItems: 0, total: 5 },
                intelligence: { base: 5, max: 9, throughItems: 0, total: 5 },
                armor: 0,
                damage: {
                    total: { low: 0, high: 2 },
                    base: { low: 0, high: 2 },
                    throughItems: 0,
                    throughStrength: 0
                },
                resilience: {
                    ceiling: 0,
                    total: 0,
                    throughItems: 0,
                    throughAgility: 0,
                },
                avoidCriticalHitChance: 0,
                blocking: {
                    ceiling: 0,
                    total: 0,
                    throughItems: 0,
                    throughStrength: 0,
                },
                blockingChance: 0,
                criticalHit: {
                    ceiling: 0,
                    total: 0,
                    throughItems: 0,
                    throughDexterity: 0,
                },
                criticalHitChance: 0,
                threat: {
                    total: 0,
                    throughItems: 0,
                    throughCharisma: 0,
                },
                healing: {
                    total: 0,
                    throughItems: 0,
                    throughIntelligence: 0,
                },
                criticalHealing: {
                    total: 0,
                    ceiling: 0,
                    throughItems: 0,
                    throughIntelligence: 0,
                },
                criticalHealingChance: 0,
            },

            profileLink: "",
            profileDataLoading: false,

            showDetailedResults: false,

            baseStats,
            prefixesStats: Object.keys(prefixesStatsRaw)
                .filter(prefixId => !excludedPrefixes.includes(parseInt(prefixId)))
                .map(prefixId => ({
                    id: prefixId,
                    name: this.$t("prefix-" + prefixId) + ` (${prefixesStatsRaw[prefixId][statsColumnsMapping.level]} lvl)`
                }))
                .sort((a, b) => a.name.localeCompare(b.name)),

            suffixesStats: Object.keys(suffixesStatsRaw)
                .filter(suffixId => !excludedSuffixes.includes(parseInt(suffixId)))
                .map(suffixId => ({
                    id: suffixId,
                    name: this.$t("suffix-" + suffixId) + ` (${suffixesStatsRaw[suffixId][statsColumnsMapping.level]} lvl)`
                }))
                .sort((a, b) => a.name.localeCompare(b.name)),
        }
    },
    mounted() {

    },

    methods: {
        statAsString,
        itemName(prefixId, base, suffixId) {
            let name = prefixId ? this.$t("prefix-" + prefixId) + " " : ""
            name += this.$t("item-" + base)
            name += suffixId ? " " + this.$t("suffix-" + suffixId) : ""

            return name
        },

        stripPercentageFromName(stat) {
            return stat.match(/Percentage$/) ? stat.replace("Percentage", "") : stat
        },

        async loadUserData() {
            this.profileDataLoading = true;
            try {
                await this.fetchData()
                this.calculateResults()
            } finally {
                this.profileDataLoading = false;
            }
        },

        async fetchData() {
            let url = null
            try {
                url = new URL(this.profileLink)
            } catch (e) {
                alert(this.$t("Profile link is not valid"))
                return
            }

            const server = (/s\d+-[a-z]+/).exec(url.host)
            const playerId = url.searchParams.get("p")
            const doll = url.searchParams.get("doll") ?? 1

            if (!server || typeof server[0] === "undefined" || !playerId || !playerId.match(/\d+/)) {
                alert(this.$t("Profile link is not valid"))
                return
            }

            let req = null
            try {
                req = await fetch(`https://fetch-profile.gladiatus-toolbox.net/?server=${server}&user_id=${playerId}&doll=${doll}`)
            } catch (e) {
                alert(this.$t("An error occurred during data loading. Try again later."))
            }

            if (!req || !req.ok) {
                alert(this.$t("Profile data could not be loaded. Ensure that the link is correct and try again in a couple of minutes."))
                return
            }

            const profileData = await req.json()
            this.playerStats.level = parseInt(profileData.level)
            this.playerStats.strength.base = parseInt(profileData.strength)
            this.playerStats.dexterity.base = parseInt(profileData.dexterity)
            this.playerStats.agility.base = parseInt(profileData.agility)
            this.playerStats.constitution.base = parseInt(profileData.constitution)
            this.playerStats.charisma.base = parseInt(profileData.charisma)
            this.playerStats.intelligence.base = parseInt(profileData.intelligence)

            for (const item in profileData.items) {
                this.playerItems[item].prefixId = profileData.items[item].prefix_id
                this.playerItems[item].base = profileData.items[item].base
                this.playerItems[item].suffixId = profileData.items[item].suffix_id
                this.playerItems[item].quality = profileData.items[item].quality
                this.playerItems[item].bonus = profileData.items[item].bonus ?? null
                this.playerItems[item].bonusValue = profileData.items[item].bonusValue ?? null
            }
        },

        calculateResults() {
            const cap = (num, cap) => num > cap ? cap : num
            const leftCap = (num, cap) => num < cap ? cap : num

            for (let itemId in this.playerItems) {
                const item = this.playerItems[itemId]
                this.playerItems[itemId].stats = item.base ? calculateItemStats(item.prefixId, item.base, item.suffixId, item.quality) : {}
            }

            const itemsStatsArr = Object.values(this.playerItems).map(item => toRaw(item.stats)).filter(v => Object.keys(v).length > 0)

            // Calculate basic stats and bonuses
            const possibleStats = ["strength", "dexterity", "agility", "constitution", "charisma", "intelligence"]
            for (const s of possibleStats) {
                this.playerStats[s].max = this.playerStats[s].base + Math.round(this.playerStats[s].base / 2) + this.playerStats.level
                this.playerStats[s].throughItems = sumStat(itemsStatsArr, statsColumnsMapping[s])

                // Additional bonuses applied to items
                this.playerStats[s].throughItems += Object.keys(this.playerItems)
                    .filter(k => this.playerItems[k].bonus === s)
                    .reduce((acc, k) => acc += this.playerItems[k].bonusValue, 0)

                // Percentage bonuses, calculated per-item basis for 100% alignment with Gladiatus calculations
                this.playerStats[s].throughItems += itemsStatsArr.reduce(
                    (acc, item) => acc += Math.round(item.stats[statsColumnsMapping[s + "Percentage"]] * this.playerStats[s].base / 100),
                    0
                )

                this.playerStats[s].total = cap(this.playerStats[s].base + this.playerStats[s].throughItems, this.playerStats[s].max)
            }
            this.playerStats.health = (this.playerStats.level * 25) + (this.playerStats.constitution.total * 25) - 50 + sumStat(itemsStatsArr, statsColumnsMapping.health)

            // Damage
            const weapon = itemsStatsArr.filter(i => i.isWeapon)?.at(0) ?? null
            this.playerStats.damage = {
                base: {
                    low: weapon ? weapon.damageLow : 0,
                    high: weapon ? weapon.damageHigh : 2
                },
                throughItems: sumStat(itemsStatsArr, statsColumnsMapping.damage) - (weapon ? weapon.stats[statsColumnsMapping.damage] : 0),
                throughStrength: Math.floor(this.playerStats.strength.total / 10)
            }
            // +damage bonus on a weapon
            this.playerStats.damage.throughItems += this.playerItems["3"].bonus === 'damage' ? this.playerItems["3"].bonusValue : 0
            this.playerStats.damage.total = {
                low: leftCap(this.playerStats.damage.base.low + this.playerStats.damage.throughItems + this.playerStats.damage.throughStrength, 0),
                high: leftCap(this.playerStats.damage.base.high + this.playerStats.damage.throughItems + this.playerStats.damage.throughStrength, 2)
            }

            // Armor
            this.playerStats.armor = sumStat(itemsStatsArr, statsColumnsMapping.armor)
            this.playerStats.armor += Object.keys(this.playerItems)
                .filter(k => this.playerItems[k].bonus === "armor")
                .reduce((acc, k) => acc += this.playerItems[k].bonusValue, 0)

            this.playerStats.resilience = {
                ceiling: Math.floor(98 * (this.playerStats.level - 8) / 52) + 1,
                throughItems: sumStat(itemsStatsArr, statsColumnsMapping.hardening),
                throughAgility: Math.floor(this.playerStats.agility.total / 10)
            }
            this.playerStats.resilience.total = this.playerStats.resilience.throughItems + this.playerStats.resilience.throughAgility
            this.playerStats.avoidCriticalHitChance = cap(Math.round(this.playerStats.resilience.total * 52 / (this.playerStats.level - 8) / 4), 25)

            this.playerStats.blocking = {
                throughItems: sumStat(itemsStatsArr, statsColumnsMapping.blocking),
                throughStrength: Math.floor(this.playerStats.strength.total / 10),
                ceiling: Math.floor(49.5 * 6 * (this.playerStats.level - 8) / 52) + 1
            }
            this.playerStats.blocking.total = this.playerStats.blocking.throughItems + this.playerStats.blocking.throughStrength
            this.playerStats.blockingChance = cap(Math.round(this.playerStats.blocking.total * 52 / (this.playerStats.level - 8) / 6), 50)

            this.playerStats.criticalHit = {
                ceiling: Math.floor(49.5 * 5 * (this.playerStats.level - 8) / 52) + 1,
                throughItems: sumStat(itemsStatsArr, statsColumnsMapping.criticalAttack),
                throughDexterity: Math.floor(this.playerStats.dexterity.total / 10),
            }
            this.playerStats.criticalHit.total = this.playerStats.criticalHit.throughItems + this.playerStats.criticalHit.throughDexterity
            this.playerStats.criticalHitChance = cap(Math.round((this.playerStats.criticalHit.total * 52 / (this.playerStats.level - 8)) / 5), 50)

            // Threat
            this.playerStats.threat = {
                throughItems: sumStat(itemsStatsArr, statsColumnsMapping.threat),
                throughCharisma: Math.floor(this.playerStats.charisma.total * 0.7)
            }
            this.playerStats.threat.total = this.playerStats.threat.throughItems + this.playerStats.threat.throughCharisma

            // Healing
            this.playerStats.healing = {
                throughItems: sumStat(itemsStatsArr, statsColumnsMapping.healing),
                throughIntelligence: Math.floor(this.playerStats.intelligence.total * 0.8)
            }
            this.playerStats.healing.total = this.playerStats.healing.throughItems + this.playerStats.healing.throughIntelligence

            this.playerStats.criticalHealing = {
                ceiling: Math.floor(89.5 * 7 * (this.playerStats.level - 8) / 52) + 1,
                throughItems: sumStat(itemsStatsArr, statsColumnsMapping.criticalHealing),
                throughIntelligence: Math.floor(this.playerStats.intelligence.total / 5)
            }
            this.playerStats.criticalHealing.total = this.playerStats.criticalHealing.throughItems + this.playerStats.criticalHealing.throughIntelligence
            this.playerStats.criticalHealingChance = cap(Math.round((this.playerStats.criticalHealing.total * 52 / (this.playerStats.level - 8)) / 8), 50)
        },

        updateItem(itemId, item) {
            this.playerItems[itemId] = item
            this.calculateResults()
        }
    },

    watch: {
        'playerStats.level': 'calculateResults'
    }
}
