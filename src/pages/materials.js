import DataTable from "../components/DataTable";
import recipes from "../data/prefixes_suffixes_recipes.json"
import prefixesRecipes from "../data/prefixes_recipes.json"
import suffixesRecipes from "../data/suffixes_recipes.json"
import excludedPrefixes from "../data/excluded_prefixes.json"
import excludedSuffixes from "../data/excluded_suffixes.json"

export default {
    name: "Materials",
    components: { DataTable },
    template: `
      <main class="w-full flex-grow p-6" v-cloak>
        <h1 class="text-3xl h1-text-color pb-6 ">{{ $t("Materials") }}</h1>

        <div class="bg-white p-6 px-5 rounded-md ">
          <div class="flex flex-col gap-y-4 md:flex-row flex-wrap" v-if="!selectedResource">
            <div class="w-full px-2 bg-gray-100 py-3 px-4 rounded-sm" v-for="group in Object.keys(resources)">
              <div class="font-bold mb-4">{{ $t("resources-group-" + group) }}</div>
              <div class="flex flex-col md:flex-row flex-wrap gap-4">
                <div v-for="material in resources[group]">
                  <a :href="'#'+material" class="flex flex-col items-center" @click="selectedResource=material">
                    <img :src="'static/resources/resource-18-' + material + '.png'" width="32" height="32">
                    {{ $t("resource-18-" + material) }}
                  </a>
                </div>
              </div>
            </div>
          </div>
          <div v-else>
            <div class="flex flex-col sm:flex-row mb-8">
              <a href="javascript:history.back()" class="w-full sm:w-8">
                <div class="w-full sm:w-8 h-8 bg-gray-100 px-3 rounded-md text-3xl btn-chevron-left"></div>
              </a>
              <div class="flex items-center mt-4 sm:mt-0 sm:ml-4">
                <img :src="'static/resources/resource-18-' + selectedResource + '.png'" width="32" height="32"
                     class="mr-2">
                <b>{{ $t("resource-18-" + selectedResource) }}</b>
              </div>
            </div>

            <div class=" py-3 px-4 rounded-sm w-full lg:w-1/2" v-if="prefixesContainingResource.length > 0">
              <b>{{ $t("Prefixes containing material") }}</b>
              <DataTable
                  :data="prefixesContainingResource"
                  :columns="['name', 'amount']"
                  :columns-labels="{name: $t('Name'), amount: $t('Amount')}"
                  :allow-header-switch="false"
                  default-sort-column="amount"
                  :default-sort-order="-1"
              ></DataTable>
            </div>

            <div class=" py-3 px-4 rounded-sm w-full lg:w-1/2" v-if="suffixesContainingResource.length > 0">
              <b>{{ $t("Suffixes containing material") }}</b>

              <DataTable
                  :data="suffixesContainingResource"
                  :columns="['name', 'amount']"
                  :columns-labels="{name: $t('Name'), amount: $t('Amount')}"
                  :allow-header-switch="false"
                  default-sort-column="amount"
                  :default-sort-order="-1"
              ></DataTable>
            </div>

            <div class=" py-3 px-4 rounded-sm w-full lg:w-1/2" v-if="recipesContainingResource.length > 0">
              <b>{{ $t("Recipes containing material") }} </b>
              ({{ $t("only first 15") }}, <label><input type="checkbox" v-model="showAllRecipes">
              {{ $t("show all") }}</label>)

              <DataTable
                  :data="recipesContainingResource"
                  :columns="['name', 'amount']"
                  :columns-labels="{name: $t('Name'), amount: $t('Amount')}"
                  :allow-header-switch="false"
                  default-sort-column="amount"
                  :default-sort-order="-1"
                  :max-rows="showAllRecipes ? -1 : 15"
              ></DataTable>
            </div>

          </div>
        </div>

      </main>`,

    data: () => ({
        resources: {
            materials: [1, 2, 3, 4, 13, 14, 15, 16, 17, 18, 19, 20,],
            monsterParts: [5, 6, 7, 8, 9, 10, 11, 12, 55, 58, 62, 64,],
            gemstones: [21, 22, 23, 24, 59, 63, 25, 26, 27, 28,],
            flasks: [37, 38, 39, 40, 41, 42, 43, 44, 61, 53,],
            runes: [29, 30, 31, 32, 33, 34, 35, 36, 60,],
            ores: [45, 46, 47, 48, 49, 50, 51, 52, 54, 56, 57,]
        },
        selectedResource: null,
        showAllRecipes: false,
        prefixesContainingResource: [],
        suffixesContainingResource: [],
        recipesContainingResource: [],
    }),
    mounted() {
        this.parseURLHashForMaterialPreselection();
        window.addEventListener("hashchange", this.parseURLHashForMaterialPreselection);
    },

    methods: {
        parseURLHashForMaterialPreselection() {
            if (!window.location.hash) {
                this.selectedResource = null
            }
            const hash = parseInt(window.location.hash.substring(1))
            if (hash && hash >= 1 && hash <= 64) {
                this.selectedResource = hash
            }
        },

        getRecipeName(recipeId) {
            const [prefix, suffix] = recipeId.split("-")
            const prefixName = this.$t(`prefix-${prefix}`)
            const suffixName = this.$t(`suffix-${suffix}`)

            return (prefixName === `prefix-${prefix}` ? `? (ID: ${prefix})` : prefixName) + " " + (suffixName === `suffix-${suffix}` ? `? (ID: ${suffix})` : suffixName)
        },

        retrieveRecipesContainingResource() {
            this.recipesContainingResource = Object.keys(recipes)
                .filter(p => recipes[p].hasOwnProperty(this.selectedResource))
                .filter(p => {
                    const [prefix, suffix] = p.split("-").map(r => parseInt(r));
                    return !excludedPrefixes.includes(prefix) && !excludedSuffixes.includes(suffix)
                })
                .map(p => {
                    const [prefix, suffix] = p.split("-").map(r => parseInt(r));
                    return {
                        name: {
                            link: `?p=items-builder#${prefix},1-1,${suffix}`,
                            label: this.getRecipeName(p)
                        },
                        amount: recipes[p][this.selectedResource]
                    }
                })
        }
    },

    watch: {
        selectedResource() {
            this.prefixesContainingResource = Object.keys(prefixesRecipes)
                .filter(p => prefixesRecipes[p].hasOwnProperty(this.selectedResource))
                .map(p => {
                    return {
                        name: {
                            link: `?p=items-builder#${p},1-1,0`,
                            label: this.$t("prefix-" + p)
                        },
                        amount: prefixesRecipes[p][this.selectedResource]
                    }
                })
            this.suffixesContainingResource = Object.keys(suffixesRecipes)
                .filter(p => suffixesRecipes[p].hasOwnProperty(this.selectedResource))
                .map(p => {
                    return {
                        name: {
                            link: `?p=items-builder#0,1-1,${p}`,
                            label: this.$t("suffix-" + p)
                        },
                        amount: suffixesRecipes[p][this.selectedResource]
                    }
                })

            this.retrieveRecipesContainingResource();
        },

        showAllRecipes() {
            this.retrieveRecipesContainingResource()
        }
    }
}
