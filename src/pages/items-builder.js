import { calculateItemResources, calculateItemStats } from "../item";
import baseStats from "../data/base_stats.json"
import prefixesStats from "../data/prefixes_stats.json"
import suffixesStats from "../data/suffixes_stats.json"
import excludedSuffixes from "../data/excluded_suffixes.json"
import { statAsString } from "../utils"
import DataTable from "../components/DataTable";

export default {
    name: "ItemsBuilder",
    components: { DataTable },
    template: `
      <main class="w-full flex-grow p-6" v-cloak>
        <h1 class="text-3xl text-black pb-6 ">{{ $t("Items builder") }}</h1>

        <div class="bg-white py-4 px-6 rounded-md ">
          <form class="flex gap-4 flex-col lg:flex-row">
            <div class="flex flex-col">
              <label class="mr-4 " for="prefix-selector">{{ $t("Prefix") }}</label>
              <select id="prefix-selector" v-model="selectedPrefix" class="rounded">
                <option selected :value="null"></option>
                <option v-for="prefix in prefixes" :value="prefix.id">{{ prefix.name }}</option>
              </select>
            </div>
            <div class="flex flex-col">
              <label for="base-selector" class="mr-4 ">{{ $t("Item") }}</label>
              <select id="base-selector" v-model="selectedBase" class="rounded">
                <option v-for="base in bases" :value="base.id">{{ base.name }}</option>
              </select>
            </div>
            <div class="flex flex-col">
              <label for="suffix-selector" class="mr-4 ">{{ $t("Suffix") }}</label>
              <select id="suffix-selector" v-model="selectedSuffix" class="rounded">
                <option selected :value="null"></option>
                <option v-for="suffix in suffixes" :value="suffix.id">{{ suffix.name }}</option>
              </select>
            </div>
          </form>
        </div>

        <div class="bg-white py-4 px-6 rounded-md  mt-4">
          <div class="mb-4">
            <h2 class="text-2xl text-black">{{ $t("Results") }}</h2>
            <label><input type="checkbox" v-model="conditioned"> Items fully conditioned</label>
          </div>
          <div v-if="generatedItem">
            <div class="flex flex-col gap-y-4 md:flex-row flex-wrap">
              <div class="md:w-1/2 xl:w-1/4 md:pr-4" v-for="(multiplier, key) in (conditioned? conditionedMultipliers : multipliers)">
                <div class="bg-gray-100 py-3 px-4 rounded-sm w-full">
                  <div class="font-bold" :style="{color: multipliersColors[key]}">{{ generatedItemName }}</div>
                  <div v-if="generatedItem.isWeapon">
                    {{ $t("stat-damage") }}
                    {{ minCap(Math.floor(generatedItem.damageLow * multiplier)) + "-" + Math.floor(generatedItem.damageHigh * multiplier) }}
                  </div>
                  <div v-for="(v,k) in AFFIX_STATS">
                      <span v-if="generatedItem.stats[v] !== 0 && !(generatedItem.isWeapon && k==='damage')">
                        {{ statAsString(generatedItem.stats[v], k, multiplier, $t("stat-" + stripPercentageFromName(k))) }}
                      </span>
                  </div>
                  <div>{{ $t("stat-conditioning") }} {{ conditioned ? "100%" : "0%" }}</div>
                </div>
              </div>
            </div>
            <div class="mt-8">
              <div class="flex items-center mb-2">
                <h2 class="text-xl text-black">{{ $t("Resources") }}</h2>
                <button class="ml-auto md:ml-4 px-2 py-1 rounded-md text-sm" @click="copyResources"
                        :class="{'bg-gray-200': !copiedSuccessfully, 'bg-green-500': copiedSuccessfully, 'text-white': copiedSuccessfully}">
                  {{ $t("Copy") }}
                  <span v-show="copiedSuccessfully">✔</span>
                </button>
              </div>
              <fieldset class="flex flex-col mb-2">
                <label><input type="checkbox" v-model="resourcesDiscountPumpkin">
                  {{ $t("-20% Mini-Pumpkin") }}
                </label>
                <label>
                  <input type="checkbox" v-model="resourcesDiscountGarments">
                  {{ $t("-20% Mercurius\` Robbers\`s Garments") }}
                </label>
                <select v-model="resourcesDiscountBellows" class="w-full md:w-64">
                  <option :value="0"></option>
                  <option :value="0.5">{{ $t("-50% Bellows of Austerity") }}</option>
                  <option :value="0.3">{{ $t("-30% Bellows of Austerity") }}</option>
                  <option :value="0.15">{{ $t("-15% Bellows of Austerity") }}</option>
                </select>
              </fieldset>
              <DataTable
                  class="w-full lg:w-1/4"
                  :columns="['name', 'amount']"
                  :columns-labels="{'name': $t('Name'), 'amount': $t('Amount')}"
                  :allow-header-switch="false"
                  default-sort-column="amount"
                  :default-sort-order="-1"
                  ref="resourcesTable"
                  :data="resourcesTableData">
              </DataTable>
            </div>

          </div>
          <div class="text-center text-gray-700 font-bold text-5xl mb-8 pointer-events-none select-none" v-else>
            {{ $t("Nothing yet") }}
          </div>
        </div>
      </main>`,

    data: () => ({
        bases: [],
        prefixes: [],
        suffixes: [],
        conditionedMultipliers: [2, 1.75, 1.5, 1.3, 1.15],
        multipliers: [1.75, 1.5, 1.3, 1.15, 1],
        multipliersColors: ["#ff0000", "#ff6a00", "#e303e0", "#5159f7", "#00b700", "#00b700"],
        selectedPrefix: null,
        selectedBase: "1-1",
        selectedSuffix: null,

        generatedItem: null,
        generatedItemName: "",
        generatedItemResources: {},

        resourcesTableData: [],

        resourcesDiscountPumpkin: false,
        resourcesDiscountGarments: false,
        resourcesDiscountBellows: 0,
        copiedSuccessfully: false,

        conditioned: true,
    }),
    mounted() {
        for (const base in baseStats) {
            this.bases.push(({ id: base, name: this.$t("item-" + base) + ` (${baseStats[base][0]} lvl)` }))
        }

        for (const prefix in prefixesStats) {
            this.prefixes.push({
                id: prefix,
                name: this.$t("prefix-" + prefix) + ` (${prefixesStats[prefix][this.AFFIX_STATS.level]} lvl)`
            })
        }
        this.prefixes.sort((a, b) => a.name.localeCompare(b.name))

        for (const suffix in suffixesStats) {
            this.suffixes.push({
                id: suffix,
                name: this.$t("suffix-" + suffix) + ` (${suffixesStats[suffix][this.AFFIX_STATS.level]} lvl)`
            })
        }
        this.suffixes = this.suffixes.filter(s => !excludedSuffixes.includes(parseInt(s.id)))
        this.suffixes.sort((a, b) => a.name.localeCompare(b.name))


        const [prefix, base, suffix] = document.location.hash.substring(1).split(",")
        this.selectedPrefix = prefix !== "" && prefix !== "0" ? prefix : null;
        this.selectedBase = base ?? "1-1";
        this.selectedSuffix = suffix !== "" && suffix !== "0" ? suffix : null;
    },

    methods: {
        statAsString: statAsString,

        generateItem() {
            this.generatedItem = calculateItemStats(this.selectedPrefix, this.selectedBase, this.selectedSuffix)
            this.generatedItemName = this.itemName()
            this.generateItemResources()
        },

        generateItemResources() {
            this.generatedItemResources = calculateItemResources(this.selectedPrefix, this.selectedBase, this.selectedSuffix)
        },

        updateUrl() {
            window.location.href = `#${this.selectedPrefix ?? ""},${this.selectedBase},${this.selectedSuffix ?? ""}`
        },

        itemName() {
            let name = this.selectedPrefix ? this.$t("prefix-" + this.selectedPrefix) + " " : ""
            name += this.$t("item-" + this.selectedBase)
            name += this.selectedSuffix ? " " + this.$t("suffix-" + this.selectedSuffix) : ""

            return name
        },

        minCap(i) {
            return i > 0 ? i : 1
        },

        stripPercentageFromName(stat) {
            return stat.match(/Percentage$/) ? stat.replace("Percentage", "") : stat
        },

        discountedResource(amount) {
            return this.minCap(Math.floor(
                amount * (1 - ((this.resourcesDiscountBellows) + (this.resourcesDiscountPumpkin ? 0.2 : 0) + (this.resourcesDiscountGarments ? 0.2 : 0)))
            ), 1)
        },

        copyResources() {
            let resourcesText = ""
            const sortedResources = this.$refs.resourcesTable.workableData // oops, accessing child data directly 🙈
            for (const i of sortedResources) {
                resourcesText += `${i.name.label} ×${i.amount}\n`
            }
            navigator.clipboard?.writeText(this.generatedItemName + "\n" + resourcesText)
            this.copiedSuccessfully = true;
            setTimeout(() => this.copiedSuccessfully = false, 1000)
        }
    },

    watch: {
        selectedPrefix() {
            this.generateItem()
            this.updateUrl()
        },
        selectedBase() {
            this.generateItem()
            this.updateUrl()
        },
        selectedSuffix() {
            this.generateItem()
            this.updateUrl()
        },

        resourcesDiscountBellows: 'generateItemResources',
        resourcesDiscountPumpkin: 'generateItemResources',
        resourcesDiscountGarments: 'generateItemResources',

        generatedItemResources: {
            handler() {
                this.resourcesTableData = Object.keys(this.generatedItemResources).map(k => {

                    return {

                        name: {
                            html: `<a href="?p=materials#${k}" class="underline">
                                     <img alt="${this.$t("resource-18-" + k)}" width="32" height="32" src="static/resources/resource-18-${k}.png" class="pointer-events-none select-none float-left">
                                     ${this.$t("resource-18-" + k)}
                                   </a>`,
                            label: this.$t("resource-18-" + k)
                        },
                        amount: this.discountedResource(this.generatedItemResources[k])

                    }
                })
            },
            deep: true
        }
    },

    created() {
        this.AFFIX_STATS = {
            "damage": 0,
            "armor": 1,
            "health": 2,
            "strength": 3,
            "strengthPercentage": 4,
            "dexterity": 5,
            "dexterityPercentage": 6,
            "agility": 7,
            "agilityPercentage": 8,
            "constitution": 9,
            "constitutionPercentage": 10,
            "charisma": 11,
            "charismaPercentage": 12,
            "intelligence": 13,
            "intelligencePercentage": 14,
            "healing": 15,
            "criticalHealing": 16,
            "criticalAttack": 17,
            "blocking": 18,
            "hardening": 19,
            "threat": 20,
            "level": 21
        }
    }
}
