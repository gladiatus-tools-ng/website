import { toRaw } from "vue";

export function statAsString(val, stat = "", multiplier = 1, name = "") {
    if (stat === "level") {
        return name + " " + val
    }

    let sign = val >= 0 ? "+" : ""

    return (name ? name + " " : "") + sign + (Math.floor(val * multiplier)) + (stat.match(/Percentage$/) ? "%" : "")
}

export const statsColumnsMapping = {
    "name": 22,
    "damage": 0,
    "armor": 1,
    "health": 2,
    "strength": 3,
    "strengthPercentage": 4,
    "dexterity": 5,
    "dexterityPercentage": 6,
    "agility": 7,
    "agilityPercentage": 8,
    "constitution": 9,
    "constitutionPercentage": 10,
    "charisma": 11,
    "charismaPercentage": 12,
    "intelligence": 13,
    "intelligencePercentage": 14,
    "healing": 15,
    "criticalHealing": 16,
    "criticalAttack": 17,
    "blocking": 18,
    "hardening": 19,
    "threat": 20,
    "level": 21,
}

export function mapObject(obj, callback) {
    return Object.keys(obj).reduce((ret, key) => {
        callback(ret, key)
        return ret
    }, {})
}

export function sumStat(items, stat) {
    return items.reduce((acc, i) => {
        acc += parseInt(i.stats[stat])
        return acc
    }, 0)
}

export function filterObject(o, callback) {
    for (const attr in o) {
        if (!callback(attr, o[attr])) {
            delete o[attr]
        }
    }

    return o
}

export function getObjectDeepProperty(obj, path) {
    return path.split('.').reduce((a, v) => a[v], obj);
}

export const qualityColors = {
    5: "#ff0000", 4: "#ff0000", 3: "#ff6a00", 2: "#e303e0", 1: "#5159f7", 0: "#00b700",
}
