import baseRecipes from "./data/base_recipes.json"
import baseStats from "./data/base_stats.json"
import prefixesStats from "./data/prefixes_stats.json"
import suffixesStats from "./data/suffixes_stats.json"
import affixesRecipes from "./data/prefixes_suffixes_recipes.json"
import { statsColumnsMapping } from "./utils"

// quality:
// olimp+ (red)   - 5
// olimp (red)    - 4
//       (orange) - 3
//       (purple) - 2
//       (blue)   - 1
//       (green)  - 0

export const qualityMultipliers = {
    5: 2,
    4: 1.75,
    3: 1.5,
    2: 1.3,
    1: 1.15,
    0: 1,
}

export function calculateItemStats(prefix, base, suffix, quality = 0) {
    const stats = Array(22).fill(0)
    // const prefixSuffixStats = [prefix ? prefixesStats[prefix] : [], suffix ? suffixesStats[suffix] : []]

    // Sum all stats of prefix and suffix, besides level
    for (let k in stats) {
        if (prefix) {
            stats[k] += Math.floor(parseInt(prefixesStats[prefix][k]) * (k < 21 ? qualityMultipliers[quality] : 1))
        }

        if (suffix) {
            stats[k] += Math.floor(parseInt(suffixesStats[suffix][k]) * (k < 21 ? qualityMultipliers[quality] : 1))
        }
    }

    let baseLevel = baseStats[base][0]
    let itemLevel = stats[statsColumnsMapping.level] + baseLevel
    let prefixArmor = prefix ? prefixesStats[prefix][statsColumnsMapping.armor] : 0
    let suffixArmor = suffix ? suffixesStats[suffix][statsColumnsMapping.armor] : 0
    let baseArmor = baseStats[base][3]

    stats[statsColumnsMapping.level] += baseStats[base][0]
    stats[statsColumnsMapping.armor] = prefixArmor + suffixArmor + baseArmor

    const generatedItem = { stats };
    generatedItem.isWeapon = base.match(/^1-/)
    generatedItem.isArmor = base.match(/^[23458]-/)


    // For weapons, calculate low and high damage
    if (generatedItem.isWeapon) {
        let baseLowDamage = baseStats[base][1]
        let baseHiDamage = baseStats[base][2]
        let prefixDamage = prefix ? prefixesStats[prefix][statsColumnsMapping.damage] : 0
        let suffixDamage = suffix ? suffixesStats[suffix][statsColumnsMapping.damage] : 0
        let damageLow = Math.ceil((itemLevel - baseLevel + 1) * 1.2) + baseLowDamage + 2 * (prefixDamage + suffixDamage) - 2
        let damageHigh = Math.ceil((itemLevel - baseLevel + 1) * 1.5) + baseHiDamage + 2 * (prefixDamage + suffixDamage) - 2
        generatedItem.damageLow = Math.floor((damageLow > 0 ? damageLow : 1) * qualityMultipliers[quality]) // Min-cap low damage to 1
        generatedItem.damageHigh = Math.floor(damageHigh * qualityMultipliers[quality])
    }

    // For armor items, calculate armor based on level
    // f1 and f2 are factors in the armor equations, depending on the type of armor (shield+armor / shoes / gloves / hat)
    if (generatedItem.isArmor) {
        const [f1, f2] = {
            2: [10, 20],
            3: [10, 20],
            4: [5, 40],
            5: [3, 100 / 1.5],
            8: [6, 100 / 3]
        }[base.match(/^[23458]/)[0]]
        generatedItem.stats[statsColumnsMapping.armor] += Math.floor(Math.floor(((itemLevel - baseLevel)) * (f1 + ((itemLevel - baseLevel)) / f2)))
    }

    generatedItem.stats[statsColumnsMapping.armor] = Math.floor(generatedItem.stats[statsColumnsMapping.armor] * qualityMultipliers[quality])

    // Shield, remove charisma%
    if (base.match(/^2/)) {
        generatedItem.stats[statsColumnsMapping.charismaPercentage] = 0
    }

    // Chest armor, remove agility%
    if (base.match(/^3/)) {
        generatedItem.stats[statsColumnsMapping.agilityPercentage] = 0
    }

    // Shoes, remove dexterity%
    if (base.match(/^8/)) {
        generatedItem.stats[statsColumnsMapping.dexterityPercentage] = 0
    }

    // Jewelery, remove strength%
    if (base.match(/^[69]/)) {
        generatedItem.stats[statsColumnsMapping.strengthPercentage] = 0
    }

    return generatedItem
}

export function calculateItemResources(prefix, base, suffix) {
    const required = [
        baseRecipes[base],
        affixesRecipes[(prefix ?? "0") + "-" + (suffix && (parseInt(suffix) < 100 || parseInt(suffix) > 109) ? suffix : "0")]
    ]

    const resources = {};

    for (const recipe of required) {
        for (const resource of Object.keys(recipe)) {
            if (typeof resources[resource] == "undefined") {
                resources[resource] = recipe[resource]
                continue
            }

            resources[resource] += recipe[resource]
        }
    }

    return resources
}
