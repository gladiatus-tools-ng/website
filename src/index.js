import { createApp } from 'vue/dist/vue.esm-bundler.js'
import I18n from "./plugins/i18n";
import App from "./app"

window.addEventListener("load", () => {
    const app = createApp(App)
    app.use(I18n)
    app.mount("#app")
})

