// Main Vue app
import ItemsBuilder from "./pages/items-builder";
import Prefixes from "./pages/prefixes";
import Suffixes from "./pages/suffixes";
import CharPlanner from "./pages/char-planner";
import Materials from "./pages/materials";
import Credits from "./pages/credits";

const routing = {
    "index": ItemsBuilder,
    "prefixes": Prefixes,
    "suffixes": Suffixes,
    "char-planner": CharPlanner,
    "materials": Materials,
    "credits": Credits,
}
export default {
    name: "App",
    template:
        `
          <aside class="relative bg-sidebar w-full sm:w-64 shadow-xl">
            <div class="p-5 flex items-center">
              <a href="https://gladiatus-toolbox.net/?p=index" class="text-white block text-3xl font-semibold uppercase hover:text-gray-300 mr-4">
                Gladiatus Toolbox
              </a>

              <button
                  class="text-white w-11 h-11 flex items-center justify-center text-xl nav-item border cursor-pointer ml-auto rounded sm:hidden"
                  style="min-width: 2.75rem" @click="navigationCollapsed = !navigationCollapsed">
                &equiv;
              </button>
            </div>

            <nav class="text-white text-base font-semibold py-3 sm:block" :class="{'hidden': navigationCollapsed}">
              <a href="index.html" class="flex items-center text-white py-4 pl-4 nav-item"
                 :class="{'active-nav-link': routeName==='index'}">
                {{$t("Items builder")}}
              </a>
              <a href="?p=prefixes" class="flex items-center text-white py-4 pl-4 nav-item"
                 :class="{'active-nav-link': routeName==='prefixes'}">
                {{$t("Prefixes")}}
              </a>
              <a href="?p=suffixes" class="flex items-center text-white py-4 pl-4  nav-item"
                 :class="{'active-nav-link': routeName==='suffixes'}">
                {{$t("Suffixes")}}
              </a>
              <a href="?p=char-planner"
                 class="flex items-center text-white hover:opacity-100 py-4 pl-4 nav-item"
                 :class="{'active-nav-link': routeName==='char-planner'}">
                {{$t("Character planner")}}
              </a>
              <a href="?p=materials" class="flex items-center text-white py-4 pl-4 nav-item"
                 :class="{'active-nav-link': routeName==='materials'}">
                {{$t("Materials")}}
              </a>
              <a href="?p=credits" class="flex items-center text-white py-4 pl-4 nav-item"
                 :class="{'active-nav-link': routeName==='credits'}">
                {{$t("Credits")}}
              </a>
            </nav>

            <div class="w-full mt-auto p-4 pt-0 flex">
              <select class="language-selector px-3 p-1 rounded mr-4 flex-1" v-model="language">
                <option value="en">English</option>
                <option value="pl">Polski</option>
                <option value="tr">Turkish</option>
              </select>
              <label class="ml-auto"><input type="checkbox" v-model="darkMode" role="switch" class="dark-mode-switch"></label>
            </div>
          </aside>

          <div class="w-full flex flex-col overflow-x-auto">
            <Component :is="pageComponent" class="w-full flex flex-col"></Component> <!-- Router page component -->
          </div>`,

    components: {
        ItemsBuilder,
        Prefixes,
        Suffixes,
        CharPlanner,
        Materials,
        Credits
    },
    data() {
        return {
            routeName: "",
            pageComponent: null,
            darkMode: false,
            navigationCollapsed: true,
            language: localStorage.getItem("language") ?? "en"
        }
    },

    mounted() {
        this.route()
        this.determineDarkMode()
    },

    methods: {
        route() {
            const routeUrl = new URLSearchParams(window.location.search)
            let routeName = routeUrl.get("p") ?? "index"

            if (typeof routing[routeName] == "undefined") {
                routeName = "index" // Change/redirect to 404 at some point
            }

            this.routeName = routeName;
            this.pageComponent = routing[routeName].name
        },

        determineDarkMode() {
            if (localStorage.getItem("dark-mode")) {
                this.darkMode = JSON.parse(localStorage.getItem("dark-mode"));
            } else if (window.matchMedia && window.matchMedia('(prefers-color-scheme: dark)').matches) {
                this.darkMode = true
            }
        },
    },
    watch: {
        darkMode(newVal) {
            newVal ? document.documentElement.classList.add("dark") : document.documentElement.classList.remove("dark")
            localStorage.setItem("dark-mode", newVal)
        },

        language(newVal) {
            localStorage.setItem("language", newVal)
            document.location.reload()
        }
    }
}
